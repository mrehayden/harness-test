import React, { Component } from 'react';
import Item from './Item';

import './App.css';

class App extends Component {
  componentWillMount() {
    let items = [{
      title: "Mountain Dew",
      unitPrice: 1.49,
      quantity: 1
    },
    {
      title: "Desperados",
      unitPrice: 2.50,
      quantity: 1
    },
    {
      title: "Jack Daniels",
      unitPrice: 2.99,
      quantity: 1
    }];

    this.setState({ items: items });
  }

  removeItem(index) {
    let items = this.state.items.filter((item, i) => i !== index);

    this.setState({ items: items });
  }

  removeAllItems() {
    this.setState({ items: [] });
  }

  updateQuantity(index, quantity) {
    let items = this.state.items.reduce((acc, item, i) => {
      if (index === i) {
        item.quantity = quantity;
      }
      return acc.concat([item]);
    }, []);

    this.setState({ items: items });
  }

  checkout() {
    if (this.state.items.length > 0) {
      alert('Checkout');
    }
    else {
      alert('You have no items in your basket');
    }
  }

  render() {
    let items;
    if (this.state.items.length <= 0) {
      items = <p>Your basket is empty.</p>
    }
    else {
      items = this.state.items.map((item, i) => {
        return <Item key={i} title={item.title} quantity={item.quantity} unitPrice={item.unitPrice} onRemove={() => this.removeItem(i)} onUpdateQuantity={this.updateQuantity.bind(this, i)} />
      });
    }
    let total = this.state.items.reduce((acc, item) => acc + (item.unitPrice * item.quantity), 0);
    return (
      <div className="App">
        <div className="App-checkout">
          <div className="App-checkout-body">
            {items}
          </div>
          <div className="App-checkout-footer">
            <span className="App-checkout-total">$ {total.toFixed(2)}</span>
            <button type="button" className="App-clear-button" onClick={() => this.removeAllItems()}>Clear</button>
            <button type="button" className="App-checkout-button" onClick={() => this.checkout()}>Check Out &rsaquo;</button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
