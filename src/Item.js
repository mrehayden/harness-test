import React, { Component } from 'react';

import './Item.css';

export default class extends Component {
  componentWillMount() {
    this.setState({ quantity: this.props.quantity });
  }

  updateQuantity(e) {
    let quantity = Math.min(99, Math.max(1, (parseInt(e.target.value, 10) || 1)));
    this.setState({ quantity: quantity });
    this.props.onUpdateQuantity(quantity);
  }

  render() {
    return (
      <div className="Item">
        <span className="Item-name">{this.props.title}</span>
        <input type="number" value={this.state.quantity} onChange={this.updateQuantity.bind(this)} className="Item-quantity" />
        <span className="Item-total">$ {(this.props.unitPrice * this.props.quantity).toFixed(2)}</span>
        <button type="button" className="Item-remove" onClick={this.props.onRemove}>🗙</button>
      </div>
    );
  }
}
